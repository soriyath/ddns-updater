# DDNS Updater for ChangeIp

> Logo courtesy of: <a target="_blank" href="https://icons8.com/icons/set/dns">DNS icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>

## Requirements

1. Open an account with [ChangeIp](https://www.changeip.com/accounts/aff.php?aff=3370) (free for at least on dynamic DNS record)
2. Create a subdomain to redirect to your dynamic IP
3. Create the `ddns-updater-secrets` secret to inject the configuration in the deployment in Rancher, it should have these values:
  - user: the email address used to login on ChangeIP;
  - password: the password used;
  - hostname: the hostname you created in point 2;
  - detectip: 1 if you want to autodetect the public ip, 0 otherwise;
  - ip: the ip address to set as an A record for the subdomain created in point 2;
  - interval: the interval to refresh the A record in minutes.

> This deployment uses basic authentication with an SSL connection with cURL.

## Parameters:

| Environment variable | Description                                                                                                                                                                                       | Default value |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------- |
| `USER`               | the username for the service                                                                                                                                                                      | not set       |
| `PASSWORD`           | the password or token for the service                                                                                                                                                             | not set       |
| `HOSTNAME`           | the host name that you are updating. e.g. `home.dynamic-dns.net`                                                                                                                                  | not set       |
| `IP`                 | the ip to set the A record of the hostname to                                                                                                                                                     | not set       |
| `DETECTIP`           | If this is set to 1, then the script will detect the external IP of the service on which the container is running, such as the external IP of your DSL or cable modem, will overwrite `IP` if set | not set       |
| `INTERVAL`           | the interval to refresh the A record in minutes                                                                                                                                                   | not set       |

## Build and run locally

- Build the docker image: `docker build --no-cache --tag ddns-updater .`
- Run it: `docker run -it --rm --name no-ip1 -e USER=username -e PASSWORD=yourpassword -e HOSTNAME=example.com -e DETECTIP=1 -e INTERVAL=1 ddns-updater`
